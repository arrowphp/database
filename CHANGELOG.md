# Changelog

All notable changes to `arrowphp/database` will be documented in this file.

Updates should follow the [Keep a CHANGELOG](http://keepachangelog.com/) principles.

## 0.1.20190203 - 2019-02-03

*Setup working project; setup configuration and migration CLI functionality.*

[0.1.20190203]: https://gitlab.com/arrowphp/arrow/compare/v0.1.20190203...

<!-- 

Template - see: https://keepachangelog.com/en/1.0.0/

## [0.20180000] - 2018-00-00

### Added
- Nothing

### Deprecated
- Nothing

### Fixed
- Nothing

### Removed
- Nothing

### Security
- Nothing

-->

# Arrow

[![Software License][ico-license]](LICENSE.md)
<!-- [![Build Status][ico-buildstatus]][link-pipelines] -->
<!-- [![Latest Version on Packagist][ico-version]][link-packagist] -->
<!-- [![Total Downloads][ico-downloads]][link-downloads] -->
<!-- [![Coverage Status][ico-scrutinizer]][link-scrutinizer] -->
<!-- [![Quality Score][ico-code-quality]][link-code-quality] -->

This is the Arrow Database plugin. It adds Database capability to any Arrow powered application. 

## Install

Via project composer.json

``` json
  "require": {
    "arrowphp/arrow": "@dev",
    "arrowphp/database": "@dev"
  }
```

## Usage

``` php
// Get the PDO instance of the default database connection
$pdo = $container->get(\Arrow\Database\Constant::CONTAINER_DATABASE());
// or get the PDO instance by config name
$pdo = $container->get(\Arrow\Database\Constant::CONTAINER_DATABASE('legacy-db'));
```

### Implementation

Update your existing `config.php` or add a new config file `config.database.php` and update with below config.

``` php

return [
    'package' => [
        'arrowphp/database' => [
            'Connections' => [
                'default' => [
                    // For more information on dsn, see https://phpdelusions.net/pdo#dsn
                    'dsn' => '<dsn>',
                    'user' => '<user>',
                    'password' => '<password>',
                    'attributes' => [
                        // eg \PDO::ATTR_PERSISTENT => true,
                    ],
                ],
            ],
        ],
    ],
];

```

## Change log

Please see [CHANGELOG](CHANGELOG.md) for more information on what has changed recently.

## Contributing

Please see [CONTRIBUTING](CONTRIBUTING.md) and [CODE_OF_CONDUCT](CODE_OF_CONDUCT.md) for details.

## Security

If you discover any security related issues, please contact [Chris Pennycuick][link-author] directly instead of using the issue tracker.

## Credits

- [Chris Pennycuick][link-author]

## License

The MIT License (MIT). Please see [License File](LICENSE.md) for more information.

<!-- [ico-version]: https://img.shields.io/packagist/v/arrowphp/arrow.svg?style=flat-square -->
<!-- [ico-downloads]: https://img.shields.io/packagist/dt/arrowphp/arrow.svg?style=flat-square -->
<!-- [ico-buildstatus]: https://gitlab.com/arrowphp/arrow/badges/master/build.svg -->
[ico-license]: https://img.shields.io/badge/license-MIT-brightgreen.svg?style=flat-square
<!-- [ico-scrutinizer]: https://img.shields.io/scrutinizer/coverage/g/:vendor/:package_name.svg?style=flat-square -->
<!-- [ico-code-quality]: https://img.shields.io/scrutinizer/g/:vendor/:package_name.svg?style=flat-square -->

<!-- [link-packagist]: https://packagist.org/packages/arrowphp/arrow -->
<!-- [link-downloads]: https://packagist.org/packages/arrowphp/arrow -->
[link-author]: https://gitlab.com/christopher.pennycuick
[link-contributors]: https://gitlab.com/arrowphp/database/graphs/master
<!-- [link-pipelines]: https://gitlab.com/arrowphp/arrow/pipelines -->
<!-- [link-scrutinizer]: https://scrutinizer-ci.com/g/:vendor/:package_name/code-structure -->
<!-- [link-code-quality]: https://scrutinizer-ci.com/g/:vendor/:package_name -->

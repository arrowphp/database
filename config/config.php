<?php

return [
    'Namespace' => 'Arrow\\Database',

    'Connections' => [
        'default' => [
            'dsn' => null, // eg 'pgsql:host=localhost;port=5432;dbname=mydb',
            'user' => null, // 'myuser',
            'password' => null, // 'mypassword',
            'attributes' => [
                // eg \PDO::ATTR_PERSISTENT => true,
            ],
        ],
    ],
];

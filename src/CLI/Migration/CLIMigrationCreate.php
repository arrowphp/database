<?php

declare(strict_types=1);

namespace Arrow\Database\CLI\Migration;

use Arrow\Config;

class CLIMigrationCreate
{

    private $config;

    public function __construct(Config $config)
    {
        $this->config = $config;
    }

    public function __invoke($options)
    {
        $name = current($options['words']);

        if (isset($options['args']['help'])) {
            $this->showHelp();
            return;
        } elseif (!$name) {
            $this->showHelp('A name must be provided.');
            return;
        }

        $path = $this->config->get('Path').'/migrations/';
        $filename = date('YmdHis_').$name.'.sql';
        $filepath = $path.$filename;

        if ($this->findMigrationWithName($path, $name)) {
            echo "Error: A migration with the name '$name' already exists.\n";
            return;
        }

        if (!is_dir($path)) {
            mkdir($path);
        }

        $success = @file_put_contents($filepath, "-- TODO write migration\n");

        if ($success) {
            echo "Success: The migration '$filename' has been created.\n";
        } else {
            echo "Error: An error occured creating the file '$filename'.\n";
        }
    }

    private function findMigrationWithName($path, $name)
    {
        foreach (glob($path.'*_'.$name.'.sql') as $filename) {
            return preg_match("/^\d{14}_{$name}\.sql$/", basename($filename));
        }

        return false;
    }

    private function showHelp($error = null)
    {
        echo "Create a DB migration SQL file of the format: YmdHis_<Name>.sql\n";
        echo "Usage: arrow migration:create [arguments] name\n";
        
        if ($error) {
            echo "  Error: {$error}\n";
        }

        echo "\nValues:\n";
        echo "  name    The human name of the migration. Required.\n";

        echo "\nArguments:\n";
        echo "  --help  Shows this help.\n";

        echo "\nExample:\n";
        echo "> arrow migration:create init_db\n";
    }
}

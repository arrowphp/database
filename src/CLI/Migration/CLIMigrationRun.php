<?php

declare(strict_types=1);

namespace Arrow\Database\CLI\Migration;

use Arrow\Database\Constant as Database;
use Arrow\Config;
use League\Container\Container;
use League\Container\Exception\NotFoundException;

class CLIMigrationRun
{

    private $config;
    private $container;

    public function __construct(Container $container, Config $config)
    {
        $this->container = $container;
        $this->config = $config;
    }

    public function __invoke($options)
    {
        if (isset($options['args']['help'])) {
            $this->showHelp();
            return;
        }

        try {
            $pdo = $this->container->get(Database::CONTAINER_DATABASE());
        } catch (NotFoundException $e) {
            $this->showHelp('No default connection available.');
            return;
        }

        $dryRun = isset($options['args']['dry-run']);
        if ($dryRun) {
            echo "DRY RUN - No migrations will be run.\n";
        } else {
            echo "Running...\n";
        }


        $path = $this->config->get('Path').'/migrations/';

        $migrated = array_flip($this->getCurrentMigrations($pdo));

        // glob sorts the results by default
        foreach (glob($path.'*.sql') as $filepath) {
            $filename = basename($filepath);
            if (isset($migrated[$filename])) {
                continue;
            }

            echo "{$filename}\n";

            if ($dryRun) {
                continue;
            }

            $pdo->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
            $pdo->beginTransaction();
            $pdo
                ->prepare('INSERT INTO migration (file) VALUES (?);')
                ->execute([$filename]);
            $pdo->exec(file_get_contents($filepath));
            $pdo->commit();
        }

        echo "Complete.\n";
    }

    private function getCurrentMigrations(\PDO $pdo): array
    {
        $this->createMigrationTableIfNoExists($pdo);

        $stmt = $pdo->prepare('SELECT file FROM migration;');
        $stmt->execute();
        return $stmt->fetchAll(\PDO::FETCH_COLUMN, 0);
    }

    private function createMigrationTableIfNoExists(\PDO $pdo): void
    {
        $pdo->exec(<<<SQL
CREATE TABLE IF NOT EXISTS migration ( 
    file text NOT NULL PRIMARY KEY, 
    executed timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP 
);
SQL
        );
    }

    private function showHelp($error = null)
    {
        echo "Run the DB migrations.\n";
        echo "Usage: arrow migration:run [arguments]\n";
        
        if ($error) {
            echo "  Error: {$error}\n";
        }

        echo "\nArguments:\n";
        echo "  --help      Shows this help.\n";
        echo "  --dry-run   Print the names of the migrations that will be run.\n";

        echo "\nExample:\n";
        echo "> arrow migration:run\n";
    }
}

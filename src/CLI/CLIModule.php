<?php

declare(strict_types=1);

namespace Arrow\Database\CLI;

use Arrow\Config;
use Arrow\CLI\CLI;
use Arrow\CLI\ModuleInterface as ModuleCLIInterface;
use League\Container\Container;
use Arrow\Database\CLI\Migration\CLIMigrationCreate;
use Arrow\Database\CLI\Migration\CLIMigrationRun;

class CLIModule implements ModuleCLIInterface
{

    public function registerCLI(CLI $cli, Container $container, Config $config): void
    {
        $cli->register('migration:create', 'Create a migration.', 'cli.migration.create');
        $cli->register('migration:run', 'Run the migrations.', 'cli.migration.run');
    }

    public function registerServices(Container $container, Config $config)
    {
        $container->add('cli.migration.create', CLIMigrationCreate::class)
            ->withArgument($config);
        $container->add('cli.migration.run', CLIMigrationRun::class)
            ->withArgument($container)
            ->withArgument($config);
    }
}

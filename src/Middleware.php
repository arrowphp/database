<?php

declare(strict_types=1);

namespace Arrow\Database;

use Arrow\Database\Constant as Database;
use Arrow\Application;
use Arrow\Event\ApplicationBootEvent;
use Arrow\MiddlewareInterface;

// use Propel\Runtime\Propel;
// use Propel\Runtime\Connection\ConnectionManagerSingle;

class Middleware implements MiddlewareInterface
{

    public function __invoke(Application $app): void
    {
        $this->setupDatabaseConnections($app);
    }

    private function setupDatabaseConnections(Application $app)
    {
        foreach ($app->config()->get('package.arrowphp/database.Connections') as $name => $connection) {
            $this->setupDatabaseConnection($app, $name, $connection);
        }
    }

    private function setupDatabaseConnection(Application $app, string $name, array $connection)
    {
        $pdo = new \PDO($connection['dsn'], $connection['user'], $connection['password']);
        foreach ($connection['attributes'] as $attribute => $value) {
            $pdo->setAttribute($attribute, $value);
        }

        $app->container()->add(Database::CONTAINER_DATABASE($name), $pdo, true);
    }
}

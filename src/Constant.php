<?php

namespace Arrow\Database;

class Constant
{

    // const CONTAINER_DATABASE_DEFAULT = 'app.db:default';

    // phpcs:ignore 
    function CONTAINER_DATABASE(string $name = 'default'): string
    {
        return 'app.db:'.$name;
    }
}

<?php

declare(strict_types=1);

use Arrow\Database\Middleware;
use Arrow\Database\Constant;

return function ($app) {
    $app->config()->add('Middleware', Middleware::class);
};
